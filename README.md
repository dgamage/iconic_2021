## Important Note
If you re-run this code, KMeans cluster lables may change. This will need re-interpreting accompanied DOCX file.

Find the document for explanation at [Report.docx](Report.docx)

For model selection code should be run to find optimal hyper-params [select.py](model_selection/autoencoder/select.py) 

For model selection hyper-parameter ranges should be set in [config.yaml](model_selection/autoencoder/config.yaml) 

Best hyper-parameters for the Autoencoder will get saved in [performance.log](model_selection/autoencoder/performance.log) 

SQL Queries are at [queries.sql](sqllite/queries.sql) 