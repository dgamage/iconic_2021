import yaml
import os
import pandas as pd
from tabulate import tabulate
import os
from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.insert(0,os.path.dirname(__file__)+'/../../')
from utilities import AnomalyDetector

import logging
logging.basicConfig(filename='example.log', level=logging.INFO)

np.random.seed(123)
tf.random.set_seed(1234)
tf.random.set_seed(1234)


config = yaml.load(open(os.path.dirname(__file__)+'/config.yaml'), Loader=yaml.FullLoader)

df = pd.read_csv(os.path.dirname(__file__)+config['data_file'])

if 'customer_id' in df.columns:
    df = df.drop(columns=['customer_id'])

print(df.shape)

sc          = MinMaxScaler()
data        = sc.fit_transform(df)
train_data, test_data, _, _ = train_test_split(data, data[:,0], test_size=0.3, random_state=21)


file = open(os.path.dirname(__file__)+'/performance.log','w+')
iteration = 1


for epochs in config['epochs']:
    for loss in config['loss']:
        for batch_size in config['batch_size']:
            for lrate in config['lrate']:


                autoencoder = AnomalyDetector()
                autoencoder.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lrate), loss=loss)
                history     = autoencoder.fit(  
                                                train_data, 
                                                train_data, 
                                                epochs          = epochs, 
                                                batch_size      = batch_size,
                                                validation_data = (test_data, test_data),
                                                shuffle         = True
                                            )
                test_loss = autoencoder.evaluate(test_data, test_data)
        
                file.write( 
                                    'iteration  :'+str(iteration)
                            + '\t epochs     :'+str(epochs)
                            + '\t batch_size :'+str(batch_size)
                            + '\t test_loss  :'+str(test_loss)
                            + '\t lrate      :'+str(lrate)
                            + '\n'
                            )

                file.flush()
                iteration  += 1


file.close()

print('------')
