import pandas as pd
from sklearn.preprocessing import StandardScaler, OneHotEncoder, OrdinalEncoder, MinMaxScaler
from tensorflow.keras.models import Model
import tensorflow as tf
from tensorflow.keras import layers, losses

    
class AnomalyDetector(Model):
  def __init__(self):
    super(AnomalyDetector, self).__init__()
    self.encoder = tf.keras.Sequential(     
                                        [
                                            layers.Dense(42, activation="relu")
                                            ,layers.Dense(41, activation="relu")
                                            ,layers.Dense(40, activation="relu")
                                        ]
                                       )

    self.decoder = tf.keras.Sequential(
                                        [
                                             layers.Dense(41, activation="relu")
                                            ,layers.Dense(42, activation="relu")
                                        ]
                                      )

  def call(self, x):
    encoded = self.encoder(x)
    decoded = self.decoder(encoded)
    return decoded
